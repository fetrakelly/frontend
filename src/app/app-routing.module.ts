

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {RouterModule, Routes} from '@angular/router';
import {SurveyListComponent} from './survey-list/survey-list.component';
import {SurveyDetailedComponent} from './survey-detailed/survey-detailed.component';

const appRoutes: Routes = [
  {path: '', redirectTo: 'survey-list', pathMatch: 'full'},
  {path: 'survey-list', component: SurveyListComponent},
  {path: 'survey-detailed/:code', component: SurveyDetailedComponent}

];

@NgModule({
  imports: [
    RouterModule.forRoot(appRoutes)
  ],
  exports: [
    RouterModule
  ]
})


export class AppRoutingModule {}
