import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';

import { AppComponent } from './app.component';
import { SurveyListComponent } from './survey-list/survey-list.component';
import {SurveyService} from './services/survey.service';
import {ReactiveFormsModule} from '@angular/forms';
import { SurveyDetailedComponent } from './survey-detailed/survey-detailed.component';
import {AppRoutingModule} from './app-routing.module';


@NgModule({
  declarations: [
    AppComponent,
    SurveyListComponent,
    SurveyDetailedComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    ReactiveFormsModule,
    AppRoutingModule

  ],
  providers: [SurveyService],
  bootstrap: [AppComponent]
})
export class AppModule { }
