import {TestBed, inject} from '@angular/core/testing';

import {SurveyService} from './survey.service';
import {HttpClient, HttpClientModule} from '@angular/common/http';

describe('SurveyService', () => {
  let httpClientStub: HttpClient;
  beforeEach(() => {
    httpClientStub = {
      get: jasmine.createSpy('get')
    } as any;
    TestBed.configureTestingModule({
      imports: [HttpClientModule],
      providers: [SurveyService,
        {provide: HttpClient, useValue: httpClientStub}]
    });
  });

  it('on doit récupérer la liste des enquêtes', inject([SurveyService], (service: SurveyService) => {
    service.getSurveys();
    expect(httpClientStub.get).toHaveBeenCalledWith('assets/api/list.json');
  }));

  it('on doit récupérer d\'une enquête selon le code', inject([SurveyService], (service: SurveyService) => {
    let code = 'XX1';
    service.getSurvey(code);
    expect(httpClientStub.get).toHaveBeenCalledWith(`assets/api/${code}.json`);
  }));

});
