import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {from, Observable, of} from 'rxjs';

@Injectable()
export class SurveyService {

  surveys:  Observable<any>;
  /**
   * Constructeur
   * @param http
   */
  constructor(private http: HttpClient) {
  }

  /**
   * récupération de la liste des enquêtes
   * @param filter
   */
  getSurveys(): Observable<any> {
    return this.http.get('assets/api/list.json');
  }

  /**
   * Récupération d'une enquête selon le code
   * @param code
   */
  getSurvey(code: string): Observable<any> {
    return this.http.get(`assets/api/${code}.json`);
  }

  /**
   *
   * @param {string} input
   * @returns {Observable<any>}
   * Normalement on fait le filtrage à partir de back end, ici on simule le fitrage comme celle de back end
   */
  search(input: string) {
    let resultat = of();
    switch (input) {
      case 'Paris' :
        resultat = of([{name: 'Paris', code: 'XX1'}]);
        break;
      case 'XX1' :
        resultat = of([{name: 'Paris', code: 'XX1'}]);
        break;
      case 'Melun' || 'XX3' :
        resultat = of([{name: 'Melun', code: 'XX3'}]);
        break;
      case 'XX3' :
        resultat = of([{name: 'Melun', code: 'XX3'}]);
        break;
      case 'Chartres' || 'XX2' :
      resultat = of([{name: 'Chartres', code: 'XX2'}]);
        break;
      case 'XX2' :
        resultat = of([{name: 'Chartres', code: 'XX2'}]);
        break;
      case '' :
        resultat = of([{name: 'Paris', code: 'XX1'}, {name: 'Chartres', code: 'XX2'}, {name: 'Melun', code: 'XX3'} ]);
        break;
    }
    return resultat;
  }




}
