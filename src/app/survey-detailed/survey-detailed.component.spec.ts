import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SurveyDetailedComponent } from './survey-detailed.component';

describe('SurveyDetailedComponent', () => {
  let component: SurveyDetailedComponent;
  let fixture: ComponentFixture<SurveyDetailedComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SurveyDetailedComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SurveyDetailedComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('devrait initialiser le component', () => {
    expect(component).toBeTruthy();
  });
});
