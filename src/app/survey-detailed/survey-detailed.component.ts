import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {SurveyService} from '../services/survey.service';
import {FormControl} from '@angular/forms';
@Component({
  selector: 'app-survey-detailed',
  templateUrl: './survey-detailed.component.html',
  styleUrls: ['./survey-detailed.component.css']
})
export class SurveyDetailedComponent implements OnInit {
  /**
   * initialisation des variables
   */
  surveyDetails: Array<any>;
  surveyResult_0: Array<any>;
  surveyResult_1: string;
  surveyResult_2: Array<any>;
  surveyList_0: Array<any>;
  surveyList_1: Array<any>;
  surveyList_2: Array<any>;
  listButton = new FormControl();

  /**
   * Constructeur
   * @param {SurveyService} surveyService
   * @param {ActivatedRoute} route
   * @param {Router} router
   */
  constructor( private surveyService: SurveyService, private route: ActivatedRoute, private router: Router) { }


  /**
   * * Retour vers la liste
   *
   */
  List(): void{
    this.router.navigate(['survey-list']);
  }

  /**
   * Initialisation
   */
  ngOnInit() {
    /**
     * recuperation paramètre routeur et recuperation detail liste
     * @type {any}
     */
    const code = this.route.snapshot.params['code'];
    this.surveyService.getSurvey(code).subscribe((data) => {
      this.surveyDetails = data;
      this.surveyList_0 = this.surveyDetails[0];
      this.surveyList_1 = this.surveyDetails[1];
      this.surveyList_2 = this.surveyDetails[2];
      this.surveyResult_0 = data[0].result;
      this.surveyResult_1 = data[1].result;
      this.surveyResult_2 = data[2].result;
    });
  }

}
