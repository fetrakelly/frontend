import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {SurveyListComponent} from './survey-list.component';
import {ReactiveFormsModule} from '@angular/forms';
import {SurveyService} from '../services/survey.service';
import { HttpClientModule} from '@angular/common/http';
import {of} from 'rxjs';

describe('SurveyListComponent', () => {
  let component: SurveyListComponent;
  let fixture: ComponentFixture<SurveyListComponent>;
  let surveyServiceStub: SurveyService;
  let data;

  beforeEach(async(() => {
    data = [{code: 'c1', name: 'name1'}];
    surveyServiceStub = {
      getSurveys: jasmine.createSpy('getSurveys').and.returnValue(of(data)),
      search: jasmine.createSpy('search')
    } as any;

    TestBed.configureTestingModule({
      declarations: [ SurveyListComponent ],
      imports: [ReactiveFormsModule, HttpClientModule],
      providers: [{provide: SurveyService, useValue: surveyServiceStub}]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SurveyListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('devrait initialiser le component', () => {
    component.ngOnInit();
    expect(surveyServiceStub.getSurveys).toHaveBeenCalled();
    expect(component.surveys).toEqual(data);
  });

});
