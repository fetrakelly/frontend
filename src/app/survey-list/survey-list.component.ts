import {Component, OnInit} from '@angular/core';
import {SurveyService} from '../services/survey.service';
import {FormControl} from '@angular/forms';
import {Router} from '@angular/router';
import {of} from 'rxjs';

@Component({
  selector: 'app-survey-list',
  templateUrl: './survey-list.component.html',
  styleUrls: ['./survey-list.component.css']
})
export class SurveyListComponent implements OnInit {

  /**
   * Liste des enquêtes
   */
  surveys: Array<any>;

  input = new FormControl;

  /**
   * Constructeur
   * @param {SurveyService} surveyService
   */
  constructor(private surveyService: SurveyService, private  router: Router) {
  }

  /**
   * Initialisation
   */
  ngOnInit(): void {
    /**
     * recuperation liste des enquêtes
     */
    this.surveyService.getSurveys().subscribe((data) => {
      this.surveys = data;

    });

    /**
     * recherche par code ou par name dans la liste des enquêtes
     */
    this.input.valueChanges.subscribe((input) => {
      this.surveyService.search(input).subscribe((result: Array<any>) => {
        this.surveys = result;
      });
    });


  }

  /**
   * Séléction d'une ligne pour afficher le détail
   * @param {string} code
   */
  selectLigne(code: string): void {
    this.router.navigate(['survey-detailed', code]);
  }
}
